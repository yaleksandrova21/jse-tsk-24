package ru.yaleksandrova.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.sevice.IServiceLocator;
import ru.yaleksandrova.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void execute();

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Override
    public String toString() {
        String result = "";
        String name = name();
        String arg = arg();
        String description = description();

        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += " [" + arg + "]";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }

}
