package ru.yaleksandrova.tm.api.sevice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.enumerated.Role;
import ru.yaleksandrova.tm.model.User;

public interface IAuthService {

    @NotNull
    String getCurrentUserId();

    @Nullable
    User getUser();

    void setCurrentUserId(@Nullable String userId);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    boolean isAuth();

    void checkRoles(@Nullable Role... roles);

}
