package ru.yaleksandrova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Task;
import java.util.List;
import java.util.Comparator;

public interface ITaskRepository extends IOwnerRepository<Task> {

    @NotNull
    Task findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task removeByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task startByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task startByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task startById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task finishById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task finishByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task finishByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    Task changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @NotNull
    Task changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

    @NotNull
    Task bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    @NotNull
    List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    Task unbindTaskById(@NotNull String userId, @NotNull String taskId);

    void removeAllTaskByProjectId(String userId, String projectId);

}
