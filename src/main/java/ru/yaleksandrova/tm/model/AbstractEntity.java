package ru.yaleksandrova.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.repository.AbstractRepository;

import java.util.UUID;

@Getter
@Setter
public class AbstractEntity<E extends AbstractRepository> {

    @NotNull
    protected String id = UUID.randomUUID().toString();

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

}
