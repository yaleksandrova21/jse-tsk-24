package ru.yaleksandrova.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
