package ru.yaleksandrova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.exception.entity.UserNotFoundException;
import ru.yaleksandrova.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected List<E> list = new ArrayList<>();

    @Override
    public void add(@NotNull final E entity) {
        list.add(entity);
    }

    @Override
    public void remove(@NotNull final E entity) {
        list.remove(entity);
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return list;
    }

    @NotNull
    @Override
    public List<E> findAll(Comparator<E> comparator) {
        return list.stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findById(id) != null;
    }

    @NotNull
    @Override
    public E findById(@NotNull final String id) {
        return  list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst().orElseThrow(UserNotFoundException::new);
    }

    @Nullable
    @Override
    public E findByIndex(@NotNull final Integer index) {
        return list.get(index);
    }

    @Nullable
    @Override
    public E removeById(@NotNull String id) {
        @Nullable final E entity = findById(id);
        list.remove(entity);
        return entity;
    }

    @Nullable
    @Override
    public E removeByIndex(@NotNull String userId, @NotNull final Integer index) {
        @Nullable final E entity = findByIndex(index);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

}
