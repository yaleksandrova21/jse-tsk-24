package ru.yaleksandrova.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
public class Command {

    @NotNull
    private final String name;

    @Nullable
    private final String argument;

    @NotNull
    private final String description;

    public Command(@NotNull String name, @Nullable String argument, @NotNull String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    @NotNull
    public String getName() {
        return name;
    }

    @Nullable
    public String getArgument() {
        return argument;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " ";
        if (argument != null && !argument.isEmpty()) result += " (" + argument + ") ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
