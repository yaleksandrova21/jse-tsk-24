package ru.yaleksandrova.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.model.User;
import ru.yaleksandrova.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(@NotNull final User user) {
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
