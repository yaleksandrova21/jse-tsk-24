package ru.yaleksandrova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.yaleksandrova.tm.command.AbstractCommand;
import ru.yaleksandrova.tm.util.ApplicationUtil;

public class UserCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new user";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN: ");
        @NotNull final String login = ApplicationUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        @NotNull final String password = ApplicationUtil.nextLine();
        System.out.println("ENTER EMAIL: ");
        @NotNull final String email = ApplicationUtil.nextLine();
        serviceLocator.getUserService().create(login, password, email);
    }

}
